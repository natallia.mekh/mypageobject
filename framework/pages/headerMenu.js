const HeaderMenu = function () {
  const searchIcon = '[data-test-id="close-icon"]';
  const searchInput = 'div [aria-label="Поиск по статьям"]';
  const popularTab = 'a[data-test-id="link-favorite"]';
  const creativityTab = 'div[data-test-id="link-rubric-1"]';
  const kinoSection = 'a[href="/tvorchestvo-kino/"]';

  this.performSearch = async function (page, searchText) {
    await page.click(searchIcon);
    await page.click(searchInput);
    await page.fill(searchInput, searchText);
    await page.click(searchInput);
  };

  const searchBy = "span.gcsc-find-more-on-google-query";
  this.getSearchByText = async function (page) {
    const searchByText = await page.textContent(searchBy);
    return searchByText;
  };

  this.goToPopularTab = async function (page) {
    await page.click(popularTab);
  };

  this.goToKino = async function (page) {
    await page.click(creativityTab);
    await page.click(kinoSection);
  };
};

export { HeaderMenu };
