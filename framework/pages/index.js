import { RegisterPage } from "./registerPage";
import { UserProfilePage } from "./userProfilePage";
import { HeaderMenu } from "./headerMenu";
import { HomePage } from "./homePage";

const pageProvder = () => ({
  HeaderMenu: () => new HeaderMenu(),
  HomePage: () => new HomePage(),
  RegisterPage: () => new RegisterPage(),
  UserProfilePage: () => new UserProfilePage(),
});

export { pageProvder };
