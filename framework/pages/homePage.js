const HomePage = function () {
  const logoIcon = 'a[data-test-id="brand-logo"]';
  const kinoPageTitle =
    'div[class="_3p-mueVsNX_HNtw92qN5SE _3GVpWHRkB6h4sYhNXxQpGT e9ozaMNMil0dcXDsyZi8g _3GVpWHRkB6h4sYhNXxQpGT"] h1';
  const notificationsPageTitle = 'div[data-test-id="notification-settings"] h1';

  this.clickLogo = async function (page) {
    await page.click(logoIcon);
  };

  this.pageUrl = async function (page) {
    const pageUrlText = await page.url();
    return pageUrlText;
  };

  this.getKinoPageTitle = async function (page) {
    const kinoTitleText = await page.textContent(kinoPageTitle);
    return kinoTitleText;
  };

  this.getNotificationsSettingsTitle = async function (page) {
    const notificationsPageTitleText = await page.textContent(
      notificationsPageTitle
    );
    return notificationsPageTitleText;
  };
};

export { HomePage };
