const UserProfilePage = function () {
  const userMenuIcon = 'div[data-test-id="user-menu"]';
  const profileName =
    'div[data-test-id="user-menu"] [data-test-id="menu-item-user"]';
  const notificationsSettings =
    '.wkh9-ZrrXr0E6w7EDPjse div[data-test-id="menu-item-settings"]';

  this.openProfileMenu = async function (page) {
    await page.click(userMenuIcon);
  };

  this.getProfileName = async function (page) {
    const profileNameText = await page.textContent(profileName);
    return profileNameText;
  };

  this.goToNotificationsSettings = async function (page) {
    await page.click(notificationsSettings);
  };
};

export { UserProfilePage };
