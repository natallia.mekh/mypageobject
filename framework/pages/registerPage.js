const RegisterPage = function () {
  const userMenuIcon = 'div[data-test-id="user-menu"]';
  const createAccountLink = 'div[data-test-id="create-account-button"]';
  const nameField = 'div[data-test-id="name-field"] input';
  const emailField = 'div[data-test-id="email-field"] input';
  const acceptCheckbox = "div ._2Wkb9VK7vmSvF4aODKp25Q";
  const createAccountButton =
    'div[data-test-id="sign-up-dialog"] [data-test-id="submit-button"]';
  const successMessage =
    'div[class="_30tpVi1dFdHh4HsHb5pZyr _3GVpWHRkB6h4sYhNXxQpGT _91lTtUG2z49D55L4p4IR1 _3GVpWHRkB6h4sYhNXxQpGT"] h2';
  const passwordField = 'div[data-test-id="password-field"] input';
  const loginButton =
    'div[data-test-id="login-dialog"] [data-test-id="submit-button"]';

  this.registration = async function (page, name, email) {
    await page.click(userMenuIcon);
    await page.click(createAccountLink);
    await page.click(nameField, name);
    await page.fill(nameField, name);
    await page.click(emailField, email);
    await page.fill(emailField, email);
    await page.click(acceptCheckbox);
    await page.click(createAccountButton);
  };

  this.getSuccessMessage = async function (page) {
    const successMessageText = await page.textContent(successMessage);
    return successMessageText;
  };

  this.login = async function (page, email, password) {
    await page.click(userMenuIcon);
    await page.click(emailField);
    await page.fill(emailField, email);
    await page.click(passwordField);
    await page.fill(passwordField, password);
    await page.click(loginButton);
  };
};

export { RegisterPage };
