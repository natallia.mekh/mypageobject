import playwright from 'playwright';

let browser;
let context;
let page;

async function goto() {
  await page.goto('https://www.adme.ru/');
  return page;
}

async function run() {
  browser = await playwright.chromium.launch({
    // Оторбражаем браузер или нет
    headless: false,
    // Эмуляция действий пользователя. Задержка.
    //    slowMo: 1000,
  });
  // в браузере создаем контекст
  context = await browser.newContext();
  page = await context.newPage();

  // Задать разрешение
  // await page.setViewportSize({
  //   width: 1280,
  //   height: 720,
  // });
}

async function stop() {
  // Сделать скриншот страницы
  await page.screenshot({ path: 'exampleFailed.jpg' });
  await page.close();
  await browser.close();
}

export
{
  goto, run, stop,
};
