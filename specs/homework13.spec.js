import chai, { assert } from "chai";
import { pageProvder } from "../framework/pages";
import { goto, run, stop } from "../lib/browser/browser";
import faker from "faker";

const { expect } = chai;
let page;

beforeEach(async () => {
  await run();
  page = await goto();
});

afterEach(async () => {
  await stop();
});

describe("Тесты с использованием Page Object", () => {
  it("+ Пользователь может зарегистрироваться на сайте", async () => {
    const randomName = faker.name.findName();
    const randomEmail = faker.internet.email();
    await pageProvder()
      .RegisterPage()
      .registration(page, randomName, randomEmail);
    const successRegistrationText = await pageProvder()
      .RegisterPage()
      .getSuccessMessage(page);
    expect(successRegistrationText).to.have.string("Спасибо за регистрацию");
  });

  it("+ Пользователь может авторизоваться на сайте", async () => {
    await pageProvder()
      .RegisterPage()
      .login(page, "testtestovich18@gmail.com", "wanitene");
    await pageProvder().UserProfilePage().openProfileMenu(page);
    const profileNameText = await pageProvder()
      .UserProfilePage()
      .getProfileName(page);
    expect(profileNameText).to.have.string("Test");
  });

  it("+ Авторизованный пользователь может перейти на страницу настройки уведомлений", async () => {
    await pageProvder()
      .RegisterPage()
      .login(page, "testtestovich18@gmail.com", "wanitene");
    await pageProvder().UserProfilePage().openProfileMenu(page);
    await pageProvder().UserProfilePage().goToNotificationsSettings(page);

    const notificationsPageTitle = await pageProvder()
      .HomePage()
      .getNotificationsSettingsTitle(page);

    expect(notificationsPageTitle).to.have.string("Настройка уведомлений");
  });

  it("+ Пользователь может вернуться на главную страницу, нажав на лого", async () => {
    await pageProvder().HeaderMenu().goToPopularTab(page);
    await pageProvder().HomePage().clickLogo(page);
    const pageUrlText = await pageProvder().HomePage().pageUrl(page);
    expect(pageUrlText).to.have.string("https://www.adme.ru/");
  });

  it("+ Пользователь может найти статью используя Поиск", async () => {
    await pageProvder().HeaderMenu().performSearch(page, "фильм");
    const searchText = await pageProvder().HeaderMenu().getSearchByText(page);
    expect(searchText).to.have.string("фильм");
  });

  it("+ Пользователь может перейти в раздел Кино, используя для навигации раскрывающееся меню ", async () => {
    await pageProvder().HeaderMenu().goToKino(page);

    const kinoTitleText = await pageProvder().HomePage().getKinoPageTitle(page);
    expect(kinoTitleText).to.have.string("Кино");
  });
});
